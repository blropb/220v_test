<?php namespace App;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Cache;


class Product extends Model {

    protected $fillable = ["title", "vendor_id", "short_desc", "price"];

    protected $dates = [];

    public static $another_currency = ['USD', 'KZT', 'BYN'];

    public static $default_currency = "RUB";

    public static $rules = [
        "title" => "required",
        "vendor_id" => "required",
        "price" => "numeric",
    ];

    public $timestamps = false;

    // Relationships
    public function vendor()
    {
        return $this->belongsTo('App\Vendor', 'vendor_id', 'vendor_id');
    }

    static function check_currency($currency)
    {
        $supported_currency = self::$another_currency;
        $supported_currency[] = self::$default_currency;
        if (!in_array($currency, $supported_currency) and !empty($currency))
            throw new Exception('Unsupported currency');
    }

    public static function convert_index($currency)
    {

        self::check_currency($currency);

        $index = Cache::get('currency');

        if (empty($index))
        {
            $index = [];
            $client = new Client(); //GuzzleHttp\Client
            $request = $client->get('https://www.cbr-xml-daily.ru/daily_json.js');
            $response = $request->getBody();
            $data = \GuzzleHttp\json_decode($response,true);
            foreach ($data['Valute'] as $i => $item)
            {
                if (in_array($i, self::$another_currency))
                {
                    $index[$i] = $item['Value'];
                }
            }

            Cache::put($currency, $index, time() + 60 * 30);
        }

        if ($currency == self::$default_currency or empty($currency))
        {
            return null;
        } else {
            return floatval($index[$currency]);
        }


    }


}
