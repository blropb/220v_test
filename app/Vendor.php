<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model {

    protected $fillable = ["vendor_id", "title"];

    protected $dates = [];

    public static $rules = [
        "vendor_id" => "required",
        "title" => "required",
    ];

    public $timestamps = false;

    // Relationships
    public function products()
    {
        return $this->hasMany('App\Product', 'vendor_id', 'vendor_id');
    }
}
