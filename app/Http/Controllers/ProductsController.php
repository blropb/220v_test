<?php namespace App\Http\Controllers;

use Illuminate\Http\Response;

class ProductsController extends Controller {

    const MODEL = "App\Product";

    use RESTActions;

    public function vendor($vendor, $currency='')
    {
        $m = self::MODEL;
        $val = $m::convert_index($currency);
        $model = $m::where('vendor_id','=',$vendor)->get();
        if (!($currency == $m::$default_currency or empty($currency)))
            foreach ($model as $index => $item) {
                $item->price = money_format('%.2n', ceil($item->price / $val * 100)/100);
            }
        if(is_null($model)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        return $this->respond(Response::HTTP_OK, $model);
    }

    public function convert_price()
    {

    }

}
