<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Array_;

class VendorsController extends Controller {

    const MODEL = "App\Vendor";

    use RESTActions;

    public function over($over)
    {
        $m = self::MODEL;
        $model = DB::table('products')
            ->select('vendor_id')
            ->groupBy('vendor_id')
            ->having('vendor_id', '>', intval($over))
            ->get();
        $vendors = [];
        foreach ($model as $index => $item) {
            $vendors[]=$item->vendor_id;
        }
        $model = $m::whereIn('vendor_id',$vendors)->get();
        return $this->respond(Response::HTTP_OK, $model);
    }
}
