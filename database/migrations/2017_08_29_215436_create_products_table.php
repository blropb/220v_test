<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{

    public function up()
    {
        Schema::create('products', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('vendor_id');
            $table->string('short_desc');
            $table->decimal('price', 5, 2);
            // Constraints declaration

        });
    }

    public function down()
    {
        Schema::drop('products');
    }
}
