<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{

    public function up()
    {
        Schema::create('vendors', function(Blueprint $table) {
            $table->increments('id');
            $table->string('vendor_id', 10)->unique();
            $table->text('title');
            // Constraints declaration

        });
    }

    public function down()
    {
        Schema::drop('vendors');
    }
}
