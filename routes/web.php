<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$app->get('/', function () use ($app) {
    return $app->version();
});


/**
 * Routes for resource vendor
 */
$app->get('vendor', 'VendorsController@all');
$app->get('vendor/{id}', 'VendorsController@get');
$app->get('vendor/over/{id}', 'VendorsController@over');
$app->post('vendor', 'VendorsController@add');
$app->put('vendor/{id}', 'VendorsController@put');
$app->delete('vendor/{id}', 'VendorsController@remove');

/**
 * Routes for resource product
 */
$app->get('product', 'ProductsController@all');
$app->get('product/{id}', 'ProductsController@get');
$app->get('product/vendor/{vendor}', 'ProductsController@vendor');
$app->get('product/vendor/{vendor}/{currency}', 'ProductsController@vendor');
$app->post('product', 'ProductsController@add');
$app->put('product/{id}', 'ProductsController@put');
$app->delete('product/{id}', 'ProductsController@remove');